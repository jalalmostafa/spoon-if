package com.jalal.dummy;

public class DummyClass {

    public int dummyMethod() {
        boolean a = true;
        boolean b = false;

        if (a && b) {
            System.out.println();
            return 1;
        } else {
        }

        if (a) {
            System.out.println();
            return 2;
        }
        if (b) {
            return 3;
        }

        if (true)
            return 4;

        if (false) {
            if (true) {
                return  5;
            }
            return 6;
        } else if (true) {
            return  7;
        }
        return 8;
    }
}
