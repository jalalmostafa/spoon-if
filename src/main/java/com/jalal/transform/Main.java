package com.jalal.transform;

import spoon.*;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        try {
            Launcher launcher = new Launcher();
            launcher.addInputResource(args[0]);
            launcher.addProcessor(new IfProcessor());
            launcher.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
