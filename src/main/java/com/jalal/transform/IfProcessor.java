package com.jalal.transform;

import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtIf;
import spoon.reflect.code.CtReturn;
import spoon.reflect.code.CtStatement;
import spoon.support.reflect.code.CtBlockImpl;

import java.util.Iterator;

public class IfProcessor extends AbstractProcessor<CtIf> {

    @Override
    public boolean isToBeProcessed(CtIf candidate) {
        CtStatement thenStmt = candidate.getThenStatement();
        if (thenStmt instanceof CtBlock) {
            CtReturn returnStmt = getReturnStatement((CtBlock) thenStmt);
            if (returnStmt != null) {
                return true;
            }
        }
        return false;
    }

    public void process(CtIf ctIf) {
        CtReturn statement = getReturnStatement((CtBlock) ctIf.getThenStatement());
        ctIf.replace(statement);
    }

    private CtReturn getReturnStatement(CtBlock stmt) {
        int c = 0;
        Iterator iter = stmt.iterator();
        CtStatement statement = null;
        while (iter.hasNext()) {
            c++;
            statement = (CtStatement) iter.next();
        }

        if (c == 1 && statement instanceof CtReturn) {
            return (CtReturn) statement;
        }
        return null;
    }
}
